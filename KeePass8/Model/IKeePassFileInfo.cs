﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KeePassLib.Serialization;
using Windows.Storage.Streams;
using Windows.UI.Xaml.Media.Imaging;

namespace KeePass8.Model
{
    public interface IKeePassFileInfo
    {
        Task UpdateInfo(); // Typically an async operation

        // If non static, following properties needs to raise propertyChanged Event.
        string FileName { get; }
        ulong FileSize { get; }
        BitmapImage Thumbnail { get; }
        DateTimeOffset DateModified { get; }
    }
}
