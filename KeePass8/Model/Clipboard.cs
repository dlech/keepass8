﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using KeePass8.ViewModel;
using Windows.ApplicationModel.DataTransfer;

namespace KeePass8.Model
{
    public class Clipboard
    {
        public Clipboard()
        {
            Messenger.Default.Register<ClipboardMessage>(this, CopyTo);
        }

        public void CopyTo(ClipboardMessage message)
        {
            DataPackage dataPackage = new DataPackage();
            dataPackage.RequestedOperation = DataPackageOperation.Copy;
            dataPackage.SetText(message.ContentToCopyToClipboard);
            Windows.ApplicationModel.DataTransfer.Clipboard.SetContent(dataPackage);

        }
    }

}
