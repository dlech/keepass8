﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;
using Windows.Storage;
using Windows.Storage.FileProperties;
using Windows.Storage.Streams;
using Windows.UI.Xaml.Media.Imaging;

namespace KeePass8.Model
{
    public class KeePassFileInfoDesignData : IKeePassFileInfo
    {

        public Task UpdateInfo()
        {
            return new Task(() => { });
        }

        public string FileName { get { return "My Keepass file"; } }
        public ulong FileSize { get { return 10000;}  }
        public DateTimeOffset DateModified { get { return new DateTimeOffset(2012, 05, 12, 13, 34, 01, 00, new TimeSpan()); } }

        public const string ThumbnailPropertyName = "Thumbnail";
       
        public BitmapImage Thumbnail
        {
            get
            {
                BitmapImage bitmap = new BitmapImage();
                bitmap.UriSource = new Uri("ms-appx:///Assets/MetroStyle/QuadNormal.ico", UriKind.Absolute);
                return bitmap;
            }

        }
    }
}
