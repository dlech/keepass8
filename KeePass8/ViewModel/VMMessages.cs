﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KeePassLib.Serialization;

namespace KeePass8.ViewModel
{
    /// <summary>
    /// Copy the content to the clipboard
    /// </summary>
    public class ClipboardMessage
    {
        public string ContentToCopyToClipboard;
    }

    /// <summary>
    /// Asynchroneous open file (including file picking)
    /// </summary>
    public class OpenFileMessage
    {
        public bool Success;
    }

    public class UserAuthentificationDialogMessage
    {
        // Call parameters:
        public IOConnectionInfo IOConnectionInfo;

        // Callback values:
        public bool Success;
    }

}
