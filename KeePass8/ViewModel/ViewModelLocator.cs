/*
  In App.xaml:
  <Application.Resources>
      <vm:ViewModelLocator xmlns:vm="clr-namespace:KeePass8"
                           x:Key="Locator" />
  </Application.Resources>
  
  In the View:
  DataContext="{Binding Source={StaticResource Locator}, Path=ViewModelName}"

  You can also use Blend to do all this with the tool's support.
  See http://www.galasoft.ch/mvvm
*/

using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;
using KeePass8.Model;
using KeePass8.ViewModel.Pages;
using Microsoft.Practices.ServiceLocation;

namespace KeePass8.ViewModel
{
    /// <summary>
    /// This class contains static references to all the view models in the
    /// application and provides an entry point for the bindings.
    /// </summary>
    public class ViewModelLocator
    {
        /// <summary>
        /// Initializes a new instance of the ViewModelLocator class.
        /// </summary>
        public ViewModelLocator()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);

            if (ViewModelBase.IsInDesignModeStatic)
            {
                if (!SimpleIoc.Default.IsRegistered<IKeePassModel>()) /*  Blend, for some reason, fancies initializing the ViewModelLocator more than once. */
                    SimpleIoc.Default.Register<IKeePassModel, KeePassModelDesignData>();
            }
            else
            {
                SimpleIoc.Default.Register<IKeePassModel, KeePassModel>();
            }

            SimpleIoc.Default.Register<GroupedPasswordItemVM>();
            SimpleIoc.Default.Register<EmptyPageNoFilesOpenedVM>();

            // Dialogs
            SimpleIoc.Default.Register<Clipboard>(true);
            SimpleIoc.Default.Register<OpenFileVM>(true);
            SimpleIoc.Default.Register<UserAuthentificationPageVM>(true);
        }

        public GroupedPasswordItemVM GroupedPasswordItemViewVM
        {
            get { return ServiceLocator.Current.GetInstance<GroupedPasswordItemVM>(); }
        }

        public static IKeePassModel Model
        {
            get { return ServiceLocator.Current.GetInstance<IKeePassModel>(); }
        }

        public static EmptyPageNoFilesOpenedVM EmptyPageNoFilesOpenedVM
        {
            get { return ServiceLocator.Current.GetInstance<EmptyPageNoFilesOpenedVM>(); }
        }

        public static UserAuthentificationPageVM UserAuthentificationPageVM
        {
            get { return ServiceLocator.Current.GetInstance<UserAuthentificationPageVM>(); }
        }

#if DEBUG
        public PasswordItemVM PasswordItemVM
        {
            get { return this.GroupedPasswordItemViewVM.PasswordItemGroups[0].Items[0]; }
        }
#endif


        public static void Cleanup()
        {
            // TODO Clear the ViewModels
        }
    }
}