﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KeePassLib;
using Windows.UI.Xaml.Media.Imaging;

namespace KeePass8
{
    public class IconBitmap
    {

        private PwIcon m_pwIconID;

        /// <summary>
        /// The BitmapImage of the Icon
        /// </summary>
        public BitmapImage Image { get; set; }


        /// <summary>
        /// Value of the built-in stock icon enum.
        /// </summary>

        public PwIcon Icon
        {
            get { return m_pwIconID; }

            set
            {
                string strPath;
                ConvertIconIDtoBitmapPath(value, out strPath);
                if (App.Current.Resources.ContainsKey(strPath))
                {
                    Image = App.Current.Resources[strPath] as BitmapImage;
                }
                else
                {
                    // Use default image.
                    if (App.Current.Resources.ContainsKey("IconDefault"))
                    {
                        Image = App.Current.Resources["IconDefault"] as BitmapImage;
                    }
                    else
                    {
                        throw new KeyNotFoundException();
                    }
                }
            }
        }

        /// <summary>
        /// Construct one of the built-in stock Icon.
        /// <param name="pwIcon">ID of the icon to create</param>.
        /// </summary>
        public IconBitmap(PwIcon pwIcon)
        {
            Icon = pwIcon;
        }


        private static void ConvertIconIDtoBitmapPath(PwIcon pwIcon, out String strPath)
        {
            if ((pwIcon < 0) || (pwIcon >= PwIcon.Count)) throw new IndexOutOfRangeException();
            
            if (!m_listLookupTable.ContainsKey(pwIcon)) throw new IndexOutOfRangeException();
            
            strPath = m_listLookupTable[pwIcon];
        }

        private static readonly Dictionary<PwIcon, string> m_listLookupTable = new Dictionary<PwIcon, string>() {
	{ PwIcon.Key,   "IconKey"  },
    { PwIcon.World, "IconWorld" },
	{ PwIcon.Warning, "IconWarning" },
    { PwIcon.NetworkServer, "IconNetworkServer" },
    { PwIcon.MarkedDirectory, "IconMarkedDirectory" },
    { PwIcon.UserCommunication, "IconUserCommunication" },
	{ PwIcon.Parts, "IconParts" },
    { PwIcon.Notepad, "IconNotepad" },
    { PwIcon.WorldSocket, "IconWorldSocket" },
    { PwIcon.Identity, "IconIdentity" },
    { PwIcon.PaperReady, "IconPaperReady" },
    { PwIcon.Digicam, "IconDigicam" },
    { PwIcon.IRCommunication, "IconIRCommunication" },
    { PwIcon.MultiKeys, "IconMultiKeys" },
    { PwIcon.Energy, "IconEnergy" },
    { PwIcon.Scanner, "IconScanner" },
    { PwIcon.WorldStar, "IconWorldStar" },
    { PwIcon.CDRom, "IconCDRom" },
    { PwIcon.Monitor, "IconMonitor" },
    { PwIcon.EMail, "IconEMail" },
    { PwIcon.Configuration, "IconConfiguration" },
    { PwIcon.ClipboardReady, "IconClipboardReady" },
    { PwIcon.PaperNew, "IconPaperNew" },
    { PwIcon.Screen, "IconScreen" },
    { PwIcon.EnergyCareful, "IconEnergyCareful" },
    { PwIcon.EMailBox, "IconEMailBox" },
    { PwIcon.Disk, "IconDisk" },
    { PwIcon.Drive, "IconDrive" },
    { PwIcon.PaperQ, "IconPaperQ" },
    { PwIcon.TerminalEncrypted, "IconTerminalEncrypted" },
    { PwIcon.Console, "IconConsole" },
    { PwIcon.Printer, "IconPrinter" },
    { PwIcon.ProgramIcons, "IconProgramIcons" },
    { PwIcon.Run, "IconRun" },
    { PwIcon.Settings, "IconSettings" },
	{ PwIcon.WorldComputer, "IconWorldComputer" },
    { PwIcon.Archive, "IconArchive" },
    { PwIcon.Homebanking, "IconHomebanking" },
    { PwIcon.DriveWindows, "IconDriveWindows" },
    { PwIcon.Clock, "IconClock" },
    { PwIcon.EMailSearch, "IconEmailSearch" },
    { PwIcon.PaperFlag, "IconPaperFlag" },
    { PwIcon.Memory, "IconMemory" },
    { PwIcon.TrashBin, "IconTrashBin" },
    { PwIcon.Note, "IconNote" },
    { PwIcon.Expired, "IconExpired" },
    { PwIcon.Info, "IconInfo" },
    { PwIcon.Package, "IconPackage" },
    { PwIcon.Folder, "IconFolder" },
    { PwIcon.FolderOpen, "IconFolderOpen" },
    { PwIcon.FolderPackage, "IconFolderPackage" },
	{ PwIcon.LockOpen, "IconLockOpen" },
    { PwIcon.PaperLocked, "IconPaperLocked" },
    { PwIcon.Checked, "IconChecked" },
    { PwIcon.Pen, "IconPen" },
    { PwIcon.Thumbnail, "IconThumbnail" },
    { PwIcon.Book, "IconBook" },
    { PwIcon.List, "IconList" },
    { PwIcon.UserKey, "IconUserKey" },
    { PwIcon.Tool, "IconTool" },
    { PwIcon.Home, "IconHome" },
    { PwIcon.Star, "IconStar" },
    { PwIcon.Tux, "IconTux" },
    { PwIcon.Feather, "IconFeather" },
    { PwIcon.Apple, "IconApple" },
    { PwIcon.Wiki, "IconWiki" },
    { PwIcon.Money, "IconMoney" },
    { PwIcon.Certificate, "IconCertificate" },
    { PwIcon.BlackBerry, "IconBlackBerry"}
    };

    }


}
