﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using KeePassLib.Serialization;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace KeePass8.ViewModel.Pages
{
    public class OpenFileVM
    {
        private NotificationMessageAction<OpenFileMessage> _openFileMessage;

        public OpenFileVM()
        {
            Messenger.Default.Register<NotificationMessageAction<OpenFileMessage>>(this,PickFile);
        }

     /// <summary>
     /// This private function is triggered by a message with a type OpenFileMessage
     /// </summary>
        private async void PickFile(NotificationMessageAction<OpenFileMessage> message)
        {
            _openFileMessage = message;
            FileOpenPicker openPicker = new FileOpenPicker();
            openPicker.ViewMode = PickerViewMode.List;
            openPicker.SuggestedStartLocation = PickerLocationId.DocumentsLibrary;
            openPicker.FileTypeFilter.Add(".kdbx");
            StorageFile file = await openPicker.PickSingleFileAsync();
            if (file == null)
            {
                message.Execute(new OpenFileMessage { Success = false }); // Message Callback
                return;
            }

            IOConnectionInfo ioc = IOConnectionInfo.FromPath(file.Path);

            Messenger.Default.Send((new NotificationMessageActionWithParameter<UserAuthentificationDialogMessage>(
                                       new UserAuthentificationDialogMessage { IOConnectionInfo = ioc },
                                       UserAuthentificationDialogCallback)));
        }

        private void UserAuthentificationDialogCallback(UserAuthentificationDialogMessage message)
        {
            if (!message.Success)
            {
                // reopen the file picker
                PickFile(_openFileMessage);
            }
            else _openFileMessage.Execute(new OpenFileMessage { Success = message.Success }); // Message Callback
        }
    }
}
