﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using KeePass8.Model;
using KeePassLib;
using KeePassLib.Cryptography;
using KeePassLib.Keys;
using KeePassLib.Serialization;
using Windows.ApplicationModel.Resources;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace KeePass8.ViewModel.Pages
{
    public class UserAuthentificationPageVM : ViewModelBase
    {

        private NotificationMessageActionWithParameter<UserAuthentificationDialogMessage> _messageCallback;
        private IOConnectionInfo _iocInfo;
        private IKeePassModel _model;
        private enum PageStateEnum { Normal, Error };
        private PageStateEnum _internalPageState;

        private string _errorMessage = "";
        private string _typedPassword;
        private string _pageState;

        // Bindable Dependency Properties
        public const string ErrorMessagePropertyName = "ErrorMessage";
        public const string PageStatePropertyName = "PageState";


        public UserAuthentificationPageVM(IKeePassModel model)
        {
            _model = model;
            Messenger.Default.Register<NotificationMessageActionWithParameter<UserAuthentificationDialogMessage>>(this, OpenUserAuthentificationDialog);
            ProceedButtonPressedCommand = new RelayCommand(() => ProceedButtonPressed());
            BackButtonPressedCommand = new RelayCommand(() => BackButtonPressed());
            SetPageState(PageStateEnum.Normal);
#if DEBUG
            if (ViewModelBase.IsInDesignModeStatic)
              FileInfo = new KeePassFileInfoDesignData();
#endif

        }


        /// <summary>
        /// Sets and gets the ErrorMessage property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public string ErrorMessage
        {
            get
            {
#if DEBUG
                if (ViewModelBase.IsInDesignModeStatic)
                    return "Something awfully wrong happened.\r\nYep. Get over it.";
#endif
                return _errorMessage;
            }

            set
            {
                if (_errorMessage == value)
                {
                    return;
                }

                RaisePropertyChanging(ErrorMessagePropertyName);
                _errorMessage = value;
                RaisePropertyChanged(ErrorMessagePropertyName);
            }
        }

        /// <summary>
        /// Sets and gets the PageState property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public string PageState
        {
            get
            {
                return _pageState;
            }

            private set
            {
                if (_pageState == value)
                {
                    return;
                }

                RaisePropertyChanging(PageStatePropertyName);
                _pageState = value;
                RaisePropertyChanged(PageStatePropertyName);
            }
        }


        /// <summary>
        /// Localized UserAuthorizationRequest string
        /// </summary>
        public string LocalizedStringUserAuthorizationRequest
        {
            get
            {
#if DEBUG
                if (ViewModelBase.IsInDesignModeStatic)
                    return "Your password please.";
#endif
                ResourceLoader rl = new ResourceLoader();
                return rl.GetString("LocalizedStringUserAuthorizationRequest");
            }
        }

        public string LocalizedStringProceed
        {
            get
            {
#if DEBUG
                if (ViewModelBase.IsInDesignModeStatic)
                    return "Proceed";
#endif
                ResourceLoader rl = new ResourceLoader();
                return rl.GetString("LocalizedStringProceed");
            }
        }
        
        public string LocalizedStringAuthentification
        {
            get
            {
#if DEBUG
                if (ViewModelBase.IsInDesignModeStatic)
                    return "Authentification";
#endif
                ResourceLoader rl = new ResourceLoader();
                return rl.GetString("LocalizedStringAuthentification");
            }
        }



        public RelayCommand ProceedButtonPressedCommand
        {
            get;
            private set;
        }

        public RelayCommand BackButtonPressedCommand
        {
            get;
            private set;
        }


        public string TypedPassword
        {
            get
            {
                return _typedPassword;
            }
            set
            {
                _typedPassword = value;
                SetPageState(PageStateEnum.Normal);  // Set back the page stage to normal, when user types in a new password.
            }
        }

        public IKeePassFileInfo FileInfo { get; set; }

        private void SetPageState(PageStateEnum state)
        {
            string stateString = "";

            switch (state)
            {
                case PageStateEnum.Normal: stateString = "Normal";
                    break;
                case PageStateEnum.Error: stateString = "ShowError";
                    break;
            }

            PageState = stateString;
            _internalPageState = state;
        }


        /// <summary>
        /// Navigates to a new page, where users can authentificate.
        /// </summary>
        /// <param name="message"></param>
        private async void OpenUserAuthentificationDialog(NotificationMessageActionWithParameter<UserAuthentificationDialogMessage> message)
        {
            _messageCallback = message;
            _iocInfo = message.CallParameter.IOConnectionInfo;

            FileInfo = new KeePassFileInfo(_iocInfo);
            // Display the UI
            Frame navigation = Window.Current.Content as Frame;
            if (navigation != null)
                navigation.Navigate(typeof(UserAuthentificationPage));

            await FileInfo.UpdateInfo();
        }

        private async void ProceedButtonPressed()
        {
            if (TypedPassword == null) return;

            byte[] pb = System.Text.Encoding.UTF8.GetBytes(TypedPassword);

            CompositeKey _pKey = new CompositeKey();
            _pKey.AddUserKey(new KcpPassword(pb));
            Array.Clear(pb, 0, pb.Length);

            PwDatabase db = await _model.OpenDatabase(_iocInfo, _pKey, true);

            if (db != null)
            {
                DismissPage(true);
            }
            else
            {
                DisplayErrorMessage(_model.ErrorCode);
            }
        }

        private void BackButtonPressed()
        {
            // Cancel the Authorization.
            DismissPage(false);
        }

        private void DismissPage(bool success)
        {
            Frame navigation = Window.Current.Content as Frame;
            if (navigation != null)
            {
                navigation.GoBack(); // Dismiss the authorization page.
                _messageCallback.CallParameter.Success = success;
                _messageCallback.Execute(_messageCallback.CallParameter);
            }
        }

        private void DisplayErrorMessage(Exception error)
        {
            ResourceLoader rl = new ResourceLoader();

            if (error as KeePassLib.Keys.InvalidCompositeKeyException != null)
            {
                ErrorMessage = rl.GetString("AuthentificationFailed");
            }
            // TO DO.
            // A bunch of other errors are known and should be handled here.
            else
            {
                ErrorMessage = rl.GetString("SorryFailed");
            }

            SetPageState(PageStateEnum.Error);

        }
    }
}
