﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;
using KeePass8.Model;
using KeePassLib;
using KeePassLib.Collections;
using Windows.UI.Core;
using Windows.UI.Xaml;

namespace KeePass8.ViewModel.Pages
{
    public class GroupedPasswordItemVM : ViewModelBase
    {
        private ObservableCollection<PasswordItemGroupVM> m_listPasswordItemGroupCollection;
        IKeePassModel m_model;

        public GroupedPasswordItemVM(IKeePassModel model)
        {
            m_model = model;
            m_listPasswordItemGroupCollection = new ObservableCollection<PasswordItemGroupVM>();
            m_model.DatabaseChanged += Model_DatabaseChanged;

            UpdateCollections();

            if (ViewModelBase.IsInDesignModeStatic)
            {
                SelectedItem = m_listPasswordItemGroupCollection[0].Items[0];
            }
        }

        void Model_DatabaseChanged(object sender, Model.DatabaseChangedEventArgs e)
        {
            UpdateCollections();
        }

        private void UpdateCollections()
        {
            m_listPasswordItemGroupCollection.Clear();

            PwDatabase pwDatabase = m_model.GetDatabase();

            if (pwDatabase.RootGroup != null)
            {

                // TO DO : Only one level of goups supported. Should test and flatten the hierarchy when the file has a different
                // group model.

                PwObjectList<PwGroup> passwordItemGroups = pwDatabase.RootGroup.GetGroups(false);
                foreach (PwGroup pwGroup in passwordItemGroups)
                {
                    PasswordItemGroupVM group = new PasswordItemGroupVM();
                    group.Title = pwGroup.Name;
                    PwObjectList<PwEntry> entries = pwGroup.GetEntries(false);
                    foreach (PwEntry item in entries)
                    {
                        group.Items.Add(new PasswordItemVM(item));
                    }

                    m_listPasswordItemGroupCollection.Add(group);
                }

                /*
                int nIconID = ((!database.RootGroup.CustomIconUuid.EqualsValue(PwUuid.Zero)) ?
                    ((int)PwIcon.Count + database.GetCustomIconIndex(
                    database.RootGroup.CustomIconUuid)) : (int)database.RootGroup.IconId);
                 * */

            }
        }

 
        public ObservableCollection<PasswordItemGroupVM> PasswordItemGroups
        {
            get
            {
                return m_listPasswordItemGroupCollection;
            }
        }

        /// <summary>
        /// The <see cref="SelectedItem" /> property's name.
        /// </summary>
        public const string SelectedItemPropertyName = "SelectedItem";

        private PasswordItemVM _passwordItemVM = null;

        /// <summary>
        /// Sets and gets the SelectedItem property. Changes the SelectedGroup as well.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public PasswordItemVM SelectedItem
        {
            get
            {
                return _passwordItemVM;
            }

            set
            {
                if (_passwordItemVM == value)
                {
                    return;
                }

                IEnumerable<PasswordItemGroupVM> query = from pwItemGroup in m_listPasswordItemGroupCollection where pwItemGroup.Items.Contains(value) select pwItemGroup;
                PasswordItemGroupVM group = query.First();
                SelectedGroup = group;
                
                RaisePropertyChanging(SelectedItemPropertyName);
                _passwordItemVM = value;
                RaisePropertyChanged(SelectedItemPropertyName);
            }
        }

        /// <summary>
        /// The <see cref="SelectedGroup" /> property's name.
        /// </summary>
        public const string SelectedGroupPropertyName = "SelectedGroup";

        private PasswordItemGroupVM _SelectedGroup = null;

        /// <summary>
        /// Sets and gets the SelectedGroup property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public PasswordItemGroupVM SelectedGroup
        {
            get { return _SelectedGroup; }

            set
            {
                if (_SelectedGroup == value) { return; }

                RaisePropertyChanging(SelectedGroupPropertyName);
                _SelectedGroup = value;
                RaisePropertyChanged(SelectedGroupPropertyName);
            }
        }


    }
}
