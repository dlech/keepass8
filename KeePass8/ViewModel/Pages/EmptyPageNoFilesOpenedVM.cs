﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using KeePassLib;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace KeePass8.ViewModel.Pages
{
    public class EmptyPageNoFilesOpenedVM : ViewModelBase
    {
        public RelayCommand OpenFileCommand
        {
            get;
            private set;
        }

        public RelayCommand CreateNewFileCommand
        {
            get;
            private set;
        }


        public EmptyPageNoFilesOpenedVM()
        {
            OpenFileCommand = new RelayCommand(() => OpenFile());
            CreateNewFileCommand = new RelayCommand(() => CreateFile());
        }

        private void OpenFile()
        {
            Messenger.Default.Send((new NotificationMessageAction<OpenFileMessage>(
                                       "Hello, call me back",
                                        OpenFileCallback)));
        }

        private void CreateFile()
        {
            // TODO
            Debug.Assert(false, "not yet implemented");
            return;
        }

        private void OpenFileCallback(OpenFileMessage message)
        {
            if (message.Success)
            {
                Frame navigation = Window.Current.Content as Frame;
                if (navigation != null)
                {
                    navigation.Navigate(typeof(GroupedPasswordItemGridPage));
                }
            }
        }

    }
}
